
package javaapplication1;
import java.sql.*;
import javax.swing.JOptionPane;

public class Database {
    
    Connection conn = null;
    
    public static Connection ConnecrDb(){
        
        try{
            // Create By Java Swing Team 2016-2019
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("LOAD DRIVER");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/swing","root","");
            System.out.println("SUCCESS DATABASE");
            return conn;
            
        }catch(Exception e){
            
            JOptionPane.showMessageDialog(null,e);
            return null;
        }
    }
    
}
